import { combineReducers } from "redux"
import { ToDoListReducer } from "./todolistReducer"


export const rootReducer = combineReducers({
    ToDoListReducer
})